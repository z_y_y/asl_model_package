#!/bin/bash
set -e

# cp -r /usr/local/anaconda3 docker/base/.
# cp -r /usr/local/freesurfer docker/base/.
cp -r ../brain_template base/asl_model_package/.
cp -r ../asl.py base/asl_model_package/.
cp -r ../predicted.sh base/asl_model_package/.
sudo docker build -f BaseDockerfile -t asl_bit_base:latest .

