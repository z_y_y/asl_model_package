#!/bin/bash
set -e

cp -r ../brain_template base/asl_model_package/.
cp -r ../asl.py base/asl_model_package/.
cp -r ../predicted.sh base/asl_model_package/.
sudo docker build -f RunDockerfile -t asl_bit:latest .

