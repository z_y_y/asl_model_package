#!/bin/bash
set -e

cp -r ../brain_template base/asl_model_package/.
cp -r ../geasl.py base/asl_model_package/.
cp -r ../predicted_ge.sh base/asl_model_package/predicted.sh
sudo docker build -f RunDockerfile -t geasl_bit:latest .

