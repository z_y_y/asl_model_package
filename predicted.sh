#!/bin/bash
set -e


export FREESURFER_HOME=/usr/local/freesurfer 
source $FREESURFER_HOME/SetUpFreeSurfer.sh
export PATH="/usr/local/anaconda3/bin:$PATH"

python3 asl.py -ap /data/asl.nii.gz