import math
import matplotlib.pyplot as plt
import numpy as np
import openpyxl
import pandas as pd
import csv
import os
from os.path import join
import shutil
import nibabel as nib
import numpy as np
import argparse
import json

# asl_path = "/home/ruikejiao_ai10/BASIL/tongxin/asl_0808/02_test/asl.nii.gz"
# t1_brainsam = "/home/ruikejiao_ai10/BASIL/tongxin/asl_0808/000brain/Atlas_182_MNI152/MNI152_T1_1mm.nii.gz"
# t1_aspects = "/home/ruikejiao_ai10/BASIL/tongxin/asl_0808/000brain/aspects_tx/3D_T1.nii.gz"
# brainsam_Atlas = "/home/ruikejiao_ai10/BASIL/tongxin/asl_0808/000brain/Atlas_182_MNI152/brain_t1/output.nii.gz"
# Arterial_Atlas = "/home/ruikejiao_ai10/BASIL/tongxin/asl_0808/000brain/arterial/ArterialAtlas.nii.gz"
# aspects_Atlas = "/home/ruikejiao_ai10/BASIL/tongxin/asl_0808/000brain/aspects_tx/aspects_label.nii.gz"
t1_brainsam = "/asl_model_package/brain_template/MNI152_T1_1mm.nii.gz"
t1_aspects = "/asl_model_package/brain_template/3D_T1.nii.gz"
brainsam_Atlas = "/asl_model_package/brain_template/output.nii.gz"
Arterial_Atlas = "/asl_model_package/brain_template/ArterialAtlas.nii.gz"
aspects_Atlas = "/asl_model_package/brain_template/aspects_label.nii.gz"




def cal_brainasl_mean(input_dir, brainregion_path, measure_name ,measure_list,label,label_code):
    seg_path = brainregion_path
    mea_dir = input_dir
    save_path = join(input_dir, measure_name + '.xlsx')

    outwb = openpyxl.Workbook()
    outws = outwb.active
    seg_list = ['fsl']
    for idx, seg in enumerate(seg_list):
        outws.cell(column=2 + idx * 5, row=1, value=seg)
        outws.cell(column=2 + idx * 5, row=2, value='Label Name')
        outws.cell(column=3 + idx * 5, row=2, value='volume')
        outws.cell(column=4 + idx * 5, row=2, value='mean_cbf')
        outws.cell(column=5 + idx * 5, row=2, value='cbf_0.5')
        outws.cell(column=6 + idx * 5, row=2, value='cbf_1.0')
        outws.cell(column=7 + idx * 5, row=2, value='cbf_1.5')
        outws.cell(column=8 + idx * 5, row=2, value='cbf_2.0')
        outws.cell(column=9 + idx * 5, row=2, value='cbf_2.5')
        outws.cell(column=10 + idx * 5, row=2, value='mean_arrival')
        outws.cell(column=11 + idx * 5, row=2, value='mean_cbv')

        seg_data = nib.load(seg_path).get_fdata()
        for t_idx, brainregion in enumerate(label):
            outws.cell(column=1, row=3 + t_idx, value=brainregion)
            outws.cell(column=2, row=3 + t_idx, value=label_code[t_idx])
            # tract_mask = seg_data[:,:,:,t_idx]
            print(t_idx, brainregion)
            # brainregion_mask = np.array(seg_data)
            brainregion_mask = np.array(seg_data == brainregion).astype('float32')
            if brainregion_mask.shape == (182, 218, 182, 1):
                brainregion_mask = brainregion_mask.reshape(182, 218, 182,)
            brainregion_v = np.sum(brainregion_mask)
            # print(brainregion_v)
            # brainregion_v = brainregion_v/brainregion
            outws.cell(column=3 + idx * 5, row=3 + t_idx, value=brainregion_v)
            # measure_list = ['cbfnot1_brainasm']
            # measure_list=['cbf',"cbf000","cbf001","cbf002","cbf003","cbf004","arrival"]
            for mea_idx, measure in enumerate(measure_list):
                mea_path = join(mea_dir, measure + '.nii.gz')
                mea_data = nib.load(mea_path).get_fdata()
                mea_data = np.round(mea_data).astype(int)
                print('*********************************')
                print(mea_data.shape,brainregion_mask.shape)
                print('*********************************')

                mea_data_tract = mea_data * brainregion_mask
                print(np.sum(mea_data_tract))
                mean_mea = np.sum(mea_data_tract) / brainregion_v

                outws.cell(column=4 + mea_idx + idx * 5, row=3 + t_idx, value=mean_mea)
    outwb.save(save_path)
    return save_path

def run_asl(asl_path,target_dir_path):
    #*********************计算总mcbf,matt,mcbv******************************************************
    os.system("fslroi "+ asl_path +" asl_M0.nii.gz 0 1")
    os.system("fslroi "+ asl_path +" asl_24.nii.gz 2 24")
    os.system("asl_file --data=asl_24.nii.gz --ntis=5 --rpts=2,2,2,3,3 --ibf=tis --iaf=tc --diff --mean=pcasl_diffdata")
    pcasl_diffdata = "pcasl_diffdata.nii.gz"
    os.system("fslmaths asl_24.nii.gz -Tmean asl_raw")
    asl_raw = "asl_raw.nii.gz"
    os.system("bet asl_raw.nii.gz asl_raw_brain")
    os.system("oxford_asl -i pcasl_diffdata -c asl_M0.nii.gz --tis 2,2.5,3,3.5,4 -o tt_rpt_cs --bolus 1.5 --t1 1.3 --t1b 1.66 --fixbolus --spatial --casl --regfrom asl_raw_brain")

    #*********************计算每个tis的cbf,att,cbv******************************************************
    os.system("asl_file --data=asl_24.nii.gz --ntis=5 --rpts=2,2,2,3,3 --ibf=tis --iaf=tc --diff --mean=pcasl_diffdata --split=pcasl_tis")
    os.system("oxford_asl -i pcasl_tis000.nii.gz -o oxasl000 --wp --casl --tis=2 --bolus=1.5  -c asl_M0.nii.gz ")
    os.system("oxford_asl -i pcasl_tis001.nii.gz -o oxasl001 --wp --casl --tis=2.5 --bolus=1.5  -c asl_M0.nii.gz ")
    os.system("oxford_asl -i pcasl_tis002.nii.gz -o oxasl002 --wp --casl --tis=3 --bolus=1.5  -c asl_M0.nii.gz ")
    os.system("oxford_asl -i pcasl_tis003.nii.gz -o oxasl003 --wp --casl --tis=3.5 --bolus=1.5  -c asl_M0.nii.gz ")
    os.system("oxford_asl -i pcasl_tis004.nii.gz -o oxasl004 --wp --casl --tis=4 --bolus=1.5  -c asl_M0.nii.gz ")

    ########################################brainsam and Arterial_Atlas no t1 配准#########################################################
    asl_m0 = "asl_M0.nii.gz"
    asltot1 = os.path.dirname(asl_path)
    cbf = join(asltot1,'tt_rpt_cs','native_space','perfusion_calib.nii.gz')
    cbf000 = join(asltot1,'oxasl000','native_space','perfusion_calib.nii.gz')
    cbf001 = join(asltot1,'oxasl001','native_space','perfusion_calib.nii.gz')
    cbf002 = join(asltot1,'oxasl002','native_space','perfusion_calib.nii.gz')
    cbf003 = join(asltot1,'oxasl003','native_space','perfusion_calib.nii.gz')
    cbf004 = join(asltot1,'oxasl004','native_space','perfusion_calib.nii.gz')
    arrival = join(asltot1,'tt_rpt_cs','native_space','arrival.nii.gz')
    cbv = join(asltot1,'tt_rpt_cs','native_space','cbv.nii.gz')
    cbf_data = nib.load(cbf).get_fdata()
    att_data = nib.load(arrival).get_fdata()
    cbv_data = cbf_data*att_data/60
    brian_data =  np.zeros((3,1))
    cbf_brain = cbf_data.sum()/(cbf_data != 0).sum()
    att_brain = att_data.sum()/(att_data != 0).sum()
    cbv_brain = cbv_data.sum()/(cbv_data != 0).sum()
    brian_data[0] = cbf_brain
    brian_data[1] = att_brain
    brian_data[2] = cbv_brain
    np.savetxt(r'brain.txt', brian_data, fmt='%.2f')
    all_json_path = os.path.join(asltot1,"all.json")
    all_brain_data = {
        "cbf":round(cbf_brain,2),
        "att":round(att_brain,2),
        "cbv":round(cbv_brain,2)
    }
    with open(all_json_path,'w') as f:
        json.dump(all_brain_data, f)
    header = nib.load(cbf)
    gt = nib.Nifti1Image(cbv_data, header.affine)
    nib.save(gt,cbv)
    os.system("flirt -in "+ asl_m0 +" -ref "+ t1_brainsam +" -cost normmi -out brainasmnot1 -omat brainasmnot1.mat")
    os.system("flirt -in "+ cbf +" -ref "+ t1_brainsam +" -interp nearestneighbour -out cbfnot1_brainasm.nii.gz -applyxfm -init brainasmnot1.mat")
    os.system("flirt -in "+ cbf000 +" -ref "+ t1_brainsam +" -interp nearestneighbour -out cbf000not1_brainasm.nii.gz -applyxfm -init brainasmnot1.mat")
    os.system("flirt -in "+ cbf001 +" -ref "+ t1_brainsam +" -interp nearestneighbour -out cbf001not1_brainasm.nii.gz -applyxfm -init brainasmnot1.mat")
    os.system("flirt -in "+ cbf002 +" -ref "+ t1_brainsam +" -interp nearestneighbour -out cbf002not1_brainasm.nii.gz -applyxfm -init brainasmnot1.mat")
    os.system("flirt -in "+ cbf003 +" -ref "+ t1_brainsam +" -interp nearestneighbour -out cbf003not1_brainasm.nii.gz -applyxfm -init brainasmnot1.mat")
    os.system("flirt -in "+ cbf004 +" -ref "+ t1_brainsam +" -interp nearestneighbour -out cbf004not1_brainasm.nii.gz -applyxfm -init brainasmnot1.mat")
    os.system("flirt -in "+ arrival +" -ref "+ t1_brainsam +" -interp nearestneighbour -out arrivalnot1_brainasm.nii.gz -applyxfm -init brainasmnot1.mat")
    os.system("flirt -in "+ cbv +" -ref "+ t1_brainsam +" -interp nearestneighbour -out cbvnot1_brainasm.nii.gz -applyxfm -init brainasmnot1.mat")
    ########################################aspects no t1 配准#########################################################
    os.system("flirt -in "+ asl_m0 +" -ref "+ t1_aspects +" -cost normmi -out aspectsnot1 -omat aspectsnot1.mat")
    os.system("flirt -in "+ cbf +" -ref "+ t1_aspects +" -interp nearestneighbour -out cbfnot1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
    os.system("flirt -in "+ cbf000 +" -ref "+ t1_aspects +" -interp nearestneighbour -out cbf000not1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
    os.system("flirt -in "+ cbf001 +" -ref "+ t1_aspects +" -interp nearestneighbour -out cbf001not1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
    os.system("flirt -in "+ cbf002 +" -ref "+ t1_aspects +" -interp nearestneighbour -out cbf002not1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
    os.system("flirt -in "+ cbf003 +" -ref "+ t1_aspects +" -interp nearestneighbour -out cbf003not1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
    os.system("flirt -in "+ cbf004 +" -ref "+ t1_aspects +" -interp nearestneighbour -out cbf004not1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
    os.system("flirt -in "+ arrival +" -ref "+ t1_aspects +" -interp nearestneighbour -out arrivalnot1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
    os.system("flirt -in "+ cbv +" -ref "+ t1_aspects +" -interp nearestneighbour -out cbvnot1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
    ########################################配准判断#########################################################
    cbfnot1_brainasm='cbfnot1_brainasm.nii.gz'
    file_size = os.stat(cbfnot1_brainasm).st_size
    file_size_mb = file_size / (1024 * 1024)
    if file_size_mb < 0.5:
        os.system("flirt -in " + t1_aspects + " -ref " + t1_brainsam + " -cost normmi -out aspectstobrainsam -omat aspectsbrainsam.mat")
        os.system("flirt -in cbfnot1_aspects.nii.gz -ref "+ t1_brainsam +" -interp nearestneighbour -out cbfnot1_brainasm.nii.gz -applyxfm -init aspectsbrainsam.mat")
        os.system("flirt -in cbf000not1_aspects.nii.gz -ref "+ t1_brainsam +" -interp nearestneighbour -out cbf000not1_brainasm.nii.gz -applyxfm -init aspectsbrainsam.mat")
        os.system("flirt -in cbf001not1_aspects.nii.gz -ref "+ t1_brainsam +" -interp nearestneighbour -out cbf001not1_brainasm.nii.gz -applyxfm -init aspectsbrainsam.mat")
        os.system("flirt -in cbf002not1_aspects.nii.gz -ref "+ t1_brainsam +" -interp nearestneighbour -out cbf002not1_brainasm.nii.gz -applyxfm -init aspectsbrainsam.mat")
        os.system("flirt -in cbf003not1_aspects.nii.gz -ref "+ t1_brainsam +" -interp nearestneighbour -out cbf003not1_brainasm.nii.gz -applyxfm -init aspectsbrainsam.mat")
        os.system("flirt -in cbf004not1_aspects.nii.gz -ref "+ t1_brainsam +" -interp nearestneighbour -out cbf004not1_brainasm.nii.gz -applyxfm -init aspectsbrainsam.mat")
        os.system("flirt -in arrivalnot1_aspects.nii.gz -ref "+ t1_brainsam +" -interp nearestneighbour -out arrivalnot1_brainasm.nii.gz -applyxfm -init aspectsbrainsam.mat")
        os.system("flirt -in cbvnot1_aspects.nii.gz -ref "+ t1_brainsam +" -interp nearestneighbour -out cbvnot1_brainasm.nii.gz -applyxfm -init aspectsbrainsam.mat")

    cbfnot1_aspects='cbfnot1_aspects.nii.gz'
    file_size = os.stat(cbfnot1_aspects).st_size
    file_size_mb = file_size / (1024 * 1024)
    if file_size_mb < 0.5:
        os.system("flirt -in "+ t1_brainsam +" -ref "+ t1_aspects +" -cost normmi -out aspectsnot1 -omat aspectsnot1.mat")
        os.system("flirt -in cbfnot1_brainasm.nii.gz -ref "+ t1_aspects +" -interp nearestneighbour -out cbfnot1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
        os.system("flirt -in cbf001not1_brainasm.nii.gz -ref "+ t1_aspects +" -interp nearestneighbour -out cbf001not1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
        os.system("flirt -in cbf002not1_brainasm.nii.gz -ref "+ t1_aspects +" -interp nearestneighbour -out cbf002not1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
        os.system("flirt -in cbf003not1_brainasm.nii.gz -ref "+ t1_aspects +" -interp nearestneighbour -out cbf003not1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
        os.system("flirt -in cbf004not1_brainasm.nii.gz -ref "+ t1_aspects +" -interp nearestneighbour -out cbf004not1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
        os.system("flirt -in arrivalnot1_brainasm.nii.gz -ref "+ t1_aspects +" -interp nearestneighbour -out arrivalnot1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
        os.system("flirt -in cbvnot1_brainasm.nii.gz -ref "+ t1_aspects +" -interp nearestneighbour -out cbvnot1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
    ########################################具体数值计算#########################################################
    brainasm = [2,3,4,5,7,8,10,11,12,13,14,15,16,17,18,24,26,28,30,31,41,42,43,44,46,47,49,50,51,52,53,54,58,60,62,63,77,85,251,252,253,254,255,
            1000,1001,1002,1003,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1019,1020,1021,1022,1023,1024,1025,1026,
            1027,1028,1029,1030,1031,1032,1033,1034,1035,2000,2001,2002,2003,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,
            2018,2019,2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030,2031,2032,2033,2034,2035]
    brainasm_label = [
        "Left-Cerebral-White-Matter",
        "Left-Cerebral-Cortex",
        "Left-Lateral-Ventricle",
        "Left-Inf-Lat-Vent",
        "Left-Cerebellum-White-Matter",
        "Left-Cerebellum-Cortex",
        "Left-Thalamus-Proper",
        "Left-Caudate",
        "Left-Putamen",
        "Left-Pallidum",
        "3rd-Ventricle",
        "4th-Ventricle",
        "Brain-Stem",
        "Left-Hippocampus",
        "Left-Amygdala",
        "CSF",
        "Left-Accumbens-area",
        "Left-VentralDC",
        "Left-vessel",
        "Left-choroid-plexus",
        "Right-Cerebral-White-Matter",
        "Right-Cerebral-Cortex",
        "Right-Lateral-Ventricle",
        "Right-Inf-Lat-Vent",
        "Right-Cerebellum-White-Matter",
        "Right-Cerebellum-Cortex",
        "Right-Thalamus-Proper",
        "Right-Caudate",
        "Right-Putamen",
        "Right-Pallidum",
        "Right-Hippocampus",
        "Right-Amygdala",
        "Right-Accumbens-area",
        "Right-VentralDC",
        "Right-vessel",
        "Right-choroid-plexus",
        "WM-hypointensities",
        "Optic-Chiasm",
        "CC_Posterior",
        "CC_Mid_Posterior",
        "CC_Central",
        "CC_Mid_Anterior",
        "CC_Anterior",
        "ctx-lh-unknown",
        "ctx-lh-bankssts",
        "ctx-lh-caudalanteriorcingulate",
        "ctx-lh-caudalmiddlefrontal",
        "ctx-lh-cuneus",
        "ctx-lh-entorhinal",
        "ctx-lh-fusiform",
        "ctx-lh-inferiorparietal",
        "ctx-lh-inferiortemporal",
        "ctx-lh-isthmuscingulate",
        "ctx-lh-lateraloccipital",
        "ctx-lh-lateralorbitofrontal",
        "ctx-lh-lingual",
        "ctx-lh-medialorbitofrontal",
        "ctx-lh-middletemporal",
        "ctx-lh-parahippocampal",
        "ctx-lh-paracentral",
        "ctx-lh-parsopercularis",
        "ctx-lh-parsorbitalis",
        "ctx-lh-parstriangularis",
        "ctx-lh-pericalcarine",
        "ctx-lh-postcentral",
        "ctx-lh-posteriorcingulate",
        "ctx-lh-precentral",
        "ctx-lh-precuneus",
        "ctx-lh-rostralanteriorcingulate",
        "ctx-lh-rostralmiddlefrontal",
        "ctx-lh-superiorfrontal",
        "ctx-lh-superiorparietal",
        "ctx-lh-superiortemporal",
        "ctx-lh-supramarginal",
        "ctx-lh-frontalpole",
        "ctx-lh-temporalpole",
        "ctx-lh-transversetemporal",
        "ctx-lh-insula",
        "ctx-rh-unknown",
        "ctx-rh-bankssts",
        "ctx-rh-caudalanteriorcingulate",
        "ctx-rh-caudalmiddlefrontal",
        "ctx-rh-cuneus",
        "ctx-rh-entorhinal",
        "ctx-rh-fusiform",
        "ctx-rh-inferiorparietal",
        "ctx-rh-inferiortemporal",
        "ctx-rh-isthmuscingulate",
        "ctx-rh-lateraloccipital",
        "ctx-rh-lateralorbitofrontal",
        "ctx-rh-lingual",
        "ctx-rh-medialorbitofrontal",
        "ctx-rh-middletemporal",
        "ctx-rh-parahippocampal",
        "ctx-rh-paracentral",
        "ctx-rh-parsopercularis",
        "ctx-rh-parsorbitalis",
        "ctx-rh-parstriangularis",
        "ctx-rh-pericalcarine",
        "ctx-rh-postcentral",
        "ctx-rh-posteriorcingulate",
        "ctx-rh-precentral",
        "ctx-rh-precuneus",
        "ctx-rh-rostralanteriorcingulate",
        "ctx-rh-rostralmiddlefrontal",
        "ctx-rh-superiorfrontal",
        "ctx-rh-superiorparietal",
        "ctx-rh-superiortemporal",
        "ctx-rh-supramarginal",
        "ctx-rh-frontalpole",
        "ctx-rh-temporalpole",
        "ctx-rh-transversetemporal",
        "ctx-rh-insula",
    ]
    aspects = [1,5,3,7,9,11,13,19,17,15,2,6,4,8,10,12,14,20,18,16]
    aspects_label = [
        "尾状核-left",
        "尾状核-right",
        "内囊-left",
        "内囊-right",
        "豆状核-left",
        "豆状核-right",
        "脑岛-left",
        "脑岛-right",
        "M1-left",
        "M1-right",
        "M2-left",
        "M2-right",
        "M3-left",
        "M3-right",
        "M4-left",
        "M4-right",
        "M5-left",
        "M5-right",
        "M6-left",
        "M6-right",
    ]
    arterial = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,20,31]
    arterial_label = [
        "anterior cerebral artery left",
        "anterior cerebral artery right",
        "medial lenticulostriate left",
        "medial lenticulostriate right",
        "lateral lenticulostriate left",
        "lateral lenticulostriate right",
        "frontal pars of middle cerebral artery left",
        "frontal pars of middle cerebral artery right",
        "parietal pars of middle cerebral artery left",
        "parietal pars of middle cerebral artery right",
        "temporal pars of middle cerebral artery left",
        "temporal pars of middle cerebral artery right",
        "occipital pars of middle cerebral artery left",
        "occipital pars of middle cerebral artery right",
        "insular pars of middle cerebral artery left",
        "insular pars of middle cerebral artery right",
        "temporal pars of posterior cerebral artery left",
        "temporal pars of posterior cerebral artery right",
        "occipital pars of posterior cerebral artery left",
        "occipital pars of posterior cerebral artery right",
        "posterior choroidal and thalamoperfurators left",
        "posterior choroidal and thalamoperfurators right",
        "anterior choroidal and thalamoperfurators left",
        "anterior choroidal and thalamoperfurators right",
        "basilar left",
        "basilar right",
        "superior cerebellar left",
        "superior cerebellar right",
        "inferior cerebellar left",
        "inferior cerebellar right",
        "lateral ventricle left",
        "lateral ventricle right",
    ]
    measure_list_brainsam =['cbfnot1_brainasm',"cbf000not1_brainasm","cbf001not1_brainasm","cbf002not1_brainasm","cbf003not1_brainasm","cbf004not1_brainasm","arrivalnot1_brainasm",'cbvnot1_brainasm']
    measure_list_Arterial = measure_list_brainsam
    measure_list_aspects =['cbfnot1_aspects','cbf000not1_aspects','cbf001not1_aspects','cbf002not1_aspects','cbf003not1_aspects','cbf004not1_aspects','arrivalnot1_aspects','cbvnot1_aspects']

    res_file_path = cal_brainasl_mean(asltot1, brainsam_Atlas, 'brainsam' ,measure_list_brainsam, brainasm,brainasm_label)
    cp_result_out_position(res_file_path,os.path.join(target_dir_path,"mean_measure_brainasm.xlsx"))
    res_file_path = cal_brainasl_mean(asltot1, Arterial_Atlas, 'Arterial' ,measure_list_Arterial, arterial,arterial_label)
    cp_result_out_position(res_file_path,os.path.join(target_dir_path,"mean_measure_arterial.xlsx"))
    res_file_path = cal_brainasl_mean(asltot1, aspects_Atlas, 'aspects' ,measure_list_aspects, aspects,aspects_label)
    cp_result_out_position(res_file_path,os.path.join(target_dir_path,"mean_measure_aspects.xlsx"))
    cp_result_out_position(all_json_path,os.path.join(target_dir_path,"all.json"))
    cp_result_out_position(cbf,os.path.join(target_dir_path,"cbf.nii.gz"))
    cp_result_out_position(cbf000,os.path.join(target_dir_path,"cbf05.nii.gz"))
    cp_result_out_position(cbf001,os.path.join(target_dir_path,"cbf10.nii.gz"))
    cp_result_out_position(cbf002,os.path.join(target_dir_path,"cbf15.nii.gz"))
    cp_result_out_position(cbf003,os.path.join(target_dir_path,"cbf20.nii.gz"))
    cp_result_out_position(cbf004,os.path.join(target_dir_path,"cbf25.nii.gz"))
    cp_result_out_position(arrival,os.path.join(target_dir_path,"att.nii.gz"))
    cp_result_out_position(cbv,os.path.join(target_dir_path,"cbv.nii.gz"))
    cp_result_out_position(os.path.join(asltot1,"cbfnot1_brainasm.nii.gz"),os.path.join(target_dir_path,"cbfnot1_brainasm.nii.gz"))
    cp_result_out_position(os.path.join(asltot1,"cbfnot1_aspects.nii.gz"),os.path.join(target_dir_path,"cbfnot1_aspects.nii.gz"))

def cp_data_in_position(asl_path):
    base_dir = os.path.dirname(__file__)
    new_asl_path = os.path.join(base_dir,"asl.nii.gz")
    shutil.copy(asl_path,new_asl_path)
    return new_asl_path

def cp_result_out_position(result_file_path,new_result_file_path):
    shutil.copy(result_file_path,new_result_file_path)
    return new_result_file_path


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="run asl model")
    parser.add_argument('-ap',"--asl_path", type = str, help = "asl.nii.gz path")
    args = parser.parse_args()
    target_dir_path = os.path.dirname(args.asl_path)
    new_asl_path = cp_data_in_position(args.asl_path)
    run_asl(new_asl_path,target_dir_path)
