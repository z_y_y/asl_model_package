import os
from os.path import join
import nibabel as nib
import numpy as np
import openpyxl
import argparse
import shutil

now_path= os.path.dirname(os.path.abspath(__file__))
t1_brainsam = "/asl_model_package/philips_template/brainasm/MNI152_T1_1mm.nii.gz"
t1_aspects = "/asl_model_package/philips_template/aspects_tx/3D_T1.nii.gz"
brainsam_Atlas = "/asl_model_package/philips_template/brainasm/output.nii.gz"
Arterial_Atlas = "/asl_model_package/philips_template/arterial/ArterialAtlas.nii.gz"
aspects_Atlas = "/asl_model_package/philips_template/aspects_tx/aspects_label.nii.gz"
# asl_m0 = join(now_path,'asl.nii.gz')
# cbf_10 = '/data7/ruikejiao/tongxin/ASL/feilipu_data/YU_WEN_HU/dicom/cbf.nii.gz'
def cal_brainasl_mean(input_dir, brainregion_path, measure_name ,measure_list,label,label_code):
    seg_path = brainregion_path
    mea_dir = input_dir
    save_path = join(input_dir, measure_name + '.xlsx')

    outwb = openpyxl.Workbook()
    outws = outwb.active
    seg_list = ['fsl']
    for idx, seg in enumerate(seg_list):
        outws.cell(column=2 + idx * 5, row=1, value=seg)
        outws.cell(column=2 + idx * 5, row=2, value='volume')
        outws.cell(column=3 + idx * 5, row=2, value='cbf_1.0')
        # outws.cell(column=4 + idx * 5, row=2, value='cbf_1.5')
        # outws.cell(column=5 + idx * 5, row=2, value='cbf_2.0')
        # outws.cell(column=6 + idx * 5, row=2, value='cbf_1.5')
        # outws.cell(column=7 + idx * 5, row=2, value='cbf_2.0')
        # outws.cell(column=8 + idx * 5, row=2, value='cbf_2.5')
        # outws.cell(column=9 + idx * 5, row=2, value='mean_arrival')
        # outws.cell(column=10 + idx * 5, row=2, value='mean_cbv')

        seg_data = nib.load(seg_path).get_fdata()
        for t_idx, brainregion in enumerate(label):
            outws.cell(column=1, row=3 + t_idx, value=brainregion)
            outws.cell(column=2, row=3 + t_idx, value=label_code[t_idx])
            # tract_mask = seg_data[:,:,:,t_idx]
            print(t_idx, brainregion)
            # brainregion_mask = np.array(seg_data)
            brainregion_mask = np.array(seg_data == brainregion).astype('float32')
            if brainregion_mask.shape == (182, 218, 182, 1):
                brainregion_mask = brainregion_mask.reshape(182, 218, 182)
            brainregion_v = np.sum(brainregion_mask)
            # print(brainregion_v)
            # brainregion_v = brainregion_v/brainregion
            outws.cell(column=3 + idx * 5, row=3 + t_idx, value=brainregion_v)
            # measure_list = ['cbfnot1_brainasm']
            # measure_list=['cbf',"cbf000","cbf001","cbf002","cbf003","cbf004","arrival"]
            for mea_idx, measure in enumerate(measure_list):
                mea_path = join(mea_dir, measure + '.nii.gz')
                mea_data = nib.load(mea_path).get_fdata()
                mea_data = np.round(mea_data).astype(int)
                print('*********************************')
                print(mea_data.shape,brainregion_mask.shape)
                print('*********************************')

                mea_data_tract = mea_data * brainregion_mask
                print(np.sum(mea_data_tract))
                mean_mea = np.sum(mea_data_tract) / brainregion_v

                outws.cell(column=3 + mea_idx + idx * 5, row=3 + t_idx, value=mean_mea)
    outwb.save(save_path)
    return save_path

def run_asl(asl_file_path:str,target_dir_path:str):
    ########################################CBF peizhun#######################################################################
    # os.system("flirt -in "+ asl_m0 +" -ref "+ t1_aspects +" -cost normmi -out aspectsnot1 -omat aspectsnot1.mat")
    # os.system("flirt -in "+ cbf_10 +" -ref "+ t1_aspects +" -interp nearestneighbour -out cbf10not1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
    # os.system("flirt -in "+ cbf_15 +" -ref "+ t1_aspects +" -interp nearestneighbour -out cbf15not1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
    # os.system("flirt -in "+ cbf_20 +" -ref "+ t1_aspects +" -interp nearestneighbour -out cbf20not1_aspects.nii.gz -applyxfm -init aspectsnot1.mat")
    os.system("flirt -in "+ asl_file_path +" -ref "+ t1_brainsam +" -cost normmi -out cbf10not1_brainsam -omat brainsamnot110.mat")
    # os.system("flirt -in "+ cbf_15 +" -ref "+ t1_brainsam +" -cost normmi -out cbf15not1_brainsam -omat brainsamnot115.mat")
    # os.system("flirt -in "+ cbf_20 +" -ref "+ t1_brainsam +" -cost normmi -out cbf20not1_brainsam -omat brainsamnot120.mat")

    os.system("flirt -in "+ asl_file_path +" -ref "+ t1_aspects +" -cost normmi -out cbf10not1_aspects -omat aspectsnot110.mat")
    # os.system("flirt -in "+ cbf_15 +" -ref "+ t1_aspects +" -cost normmi -out cbf15not1_aspects -omat aspectsnot115.mat")
    # os.system("flirt -in "+ cbf_20 +" -ref "+ t1_aspects +" -cost normmi -out cbf20not1_aspects -omat aspectsnot120.mat")
    ###########################################Calculate the average CBF value of the entire brain##################################################################
    cbf10_d = nib.load(asl_file_path)
    cbf10_data = cbf10_d.get_fdata()
    # cbf15_data = nib.load(cbf_15).get_fdata()
    # cbf20_data = nib.load(cbf_20).get_fdata()
    brian_data = np.zeros((3,1))
    cbf10_brain = cbf10_data.sum()/(cbf10_data !=0).sum()
    # cbf15_brain = cbf15_data.sum()/(cbf15_data !=0).sum()
    # cbf20_brain = cbf20_data.sum()/(cbf20_data !=0).sum()
    brian_data[0] = cbf10_brain
    # brian_data[1] = cbf15_brain
    # brian_data[2] = cbf20_brain
    np.savetxt(r'brain.txt', brian_data, fmt='%.2f')
    # ##########################################Calculate CBF values for brain regions##################################################################
    brainasm = [2,3,4,5,7,8,10,11,12,13,14,15,16,17,18,24,26,28,30,31,41,42,43,44,46,47,49,50,51,52,53,54,58,60,62,63,77,85,251,252,253,254,255,
            1000,1001,1002,1003,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1019,1020,1021,1022,1023,1024,1025,1026,
            1027,1028,1029,1030,1031,1032,1033,1034,1035,2000,2001,2002,2003,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,
            2018,2019,2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030,2031,2032,2033,2034,2035]
    brainasm_label = [
        "Left-Cerebral-White-Matter",
        "Left-Cerebral-Cortex",
        "Left-Lateral-Ventricle",
        "Left-Inf-Lat-Vent",
        "Left-Cerebellum-White-Matter",
        "Left-Cerebellum-Cortex",
        "Left-Thalamus-Proper",
        "Left-Caudate",
        "Left-Putamen",
        "Left-Pallidum",
        "3rd-Ventricle",
        "4th-Ventricle",
        "Brain-Stem",
        "Left-Hippocampus",
        "Left-Amygdala",
        "CSF",
        "Left-Accumbens-area",
        "Left-VentralDC",
        "Left-vessel",
        "Left-choroid-plexus",
        "Right-Cerebral-White-Matter",
        "Right-Cerebral-Cortex",
        "Right-Lateral-Ventricle",
        "Right-Inf-Lat-Vent",
        "Right-Cerebellum-White-Matter",
        "Right-Cerebellum-Cortex",
        "Right-Thalamus-Proper",
        "Right-Caudate",
        "Right-Putamen",
        "Right-Pallidum",
        "Right-Hippocampus",
        "Right-Amygdala",
        "Right-Accumbens-area",
        "Right-VentralDC",
        "Right-vessel",
        "Right-choroid-plexus",
        "WM-hypointensities",
        "Optic-Chiasm",
        "CC_Posterior",
        "CC_Mid_Posterior",
        "CC_Central",
        "CC_Mid_Anterior",
        "CC_Anterior",
        "ctx-lh-unknown",
        "ctx-lh-bankssts",
        "ctx-lh-caudalanteriorcingulate",
        "ctx-lh-caudalmiddlefrontal",
        "ctx-lh-cuneus",
        "ctx-lh-entorhinal",
        "ctx-lh-fusiform",
        "ctx-lh-inferiorparietal",
        "ctx-lh-inferiortemporal",
        "ctx-lh-isthmuscingulate",
        "ctx-lh-lateraloccipital",
        "ctx-lh-lateralorbitofrontal",
        "ctx-lh-lingual",
        "ctx-lh-medialorbitofrontal",
        "ctx-lh-middletemporal",
        "ctx-lh-parahippocampal",
        "ctx-lh-paracentral",
        "ctx-lh-parsopercularis",
        "ctx-lh-parsorbitalis",
        "ctx-lh-parstriangularis",
        "ctx-lh-pericalcarine",
        "ctx-lh-postcentral",
        "ctx-lh-posteriorcingulate",
        "ctx-lh-precentral",
        "ctx-lh-precuneus",
        "ctx-lh-rostralanteriorcingulate",
        "ctx-lh-rostralmiddlefrontal",
        "ctx-lh-superiorfrontal",
        "ctx-lh-superiorparietal",
        "ctx-lh-superiortemporal",
        "ctx-lh-supramarginal",
        "ctx-lh-frontalpole",
        "ctx-lh-temporalpole",
        "ctx-lh-transversetemporal",
        "ctx-lh-insula",
        "ctx-rh-unknown",
        "ctx-rh-bankssts",
        "ctx-rh-caudalanteriorcingulate",
        "ctx-rh-caudalmiddlefrontal",
        "ctx-rh-cuneus",
        "ctx-rh-entorhinal",
        "ctx-rh-fusiform",
        "ctx-rh-inferiorparietal",
        "ctx-rh-inferiortemporal",
        "ctx-rh-isthmuscingulate",
        "ctx-rh-lateraloccipital",
        "ctx-rh-lateralorbitofrontal",
        "ctx-rh-lingual",
        "ctx-rh-medialorbitofrontal",
        "ctx-rh-middletemporal",
        "ctx-rh-parahippocampal",
        "ctx-rh-paracentral",
        "ctx-rh-parsopercularis",
        "ctx-rh-parsorbitalis",
        "ctx-rh-parstriangularis",
        "ctx-rh-pericalcarine",
        "ctx-rh-postcentral",
        "ctx-rh-posteriorcingulate",
        "ctx-rh-precentral",
        "ctx-rh-precuneus",
        "ctx-rh-rostralanteriorcingulate",
        "ctx-rh-rostralmiddlefrontal",
        "ctx-rh-superiorfrontal",
        "ctx-rh-superiorparietal",
        "ctx-rh-superiortemporal",
        "ctx-rh-supramarginal",
        "ctx-rh-frontalpole",
        "ctx-rh-temporalpole",
        "ctx-rh-transversetemporal",
        "ctx-rh-insula",
    ]
    aspects = [1,5,3,7,9,11,13,19,17,15,2,6,4,8,10,12,14,20,18,16]
    aspects_label = [
        "尾状核-left",
        "尾状核-right",
        "内囊-left",
        "内囊-right",
        "豆状核-left",
        "豆状核-right",
        "脑岛-left",
        "脑岛-right",
        "M1-left",
        "M1-right",
        "M2-left",
        "M2-right",
        "M3-left",
        "M3-right",
        "M4-left",
        "M4-right",
        "M5-left",
        "M5-right",
        "M6-left",
        "M6-right",
    ]
    arterial = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,20,31,32]
    arterial_label = [
        "anterior cerebral artery left",
        "anterior cerebral artery right",
        "medial lenticulostriate left",
        "medial lenticulostriate right",
        "lateral lenticulostriate left",
        "lateral lenticulostriate right",
        "frontal pars of middle cerebral artery left",
        "frontal pars of middle cerebral artery right",
        "parietal pars of middle cerebral artery left",
        "parietal pars of middle cerebral artery right",
        "temporal pars of middle cerebral artery left",
        "temporal pars of middle cerebral artery right",
        "occipital pars of middle cerebral artery left",
        "occipital pars of middle cerebral artery right",
        "insular pars of middle cerebral artery left",
        "insular pars of middle cerebral artery right",
        "temporal pars of posterior cerebral artery left",
        "temporal pars of posterior cerebral artery right",
        "occipital pars of posterior cerebral artery left",
        "occipital pars of posterior cerebral artery right",
        "posterior choroidal and thalamoperfurators left",
        "posterior choroidal and thalamoperfurators right",
        "anterior choroidal and thalamoperfurators left",
        "anterior choroidal and thalamoperfurators right",
        "basilar left",
        "basilar right",
        "superior cerebellar left",
        "superior cerebellar right",
        "inferior cerebellar left",
        "inferior cerebellar right",
        "lateral ventricle left",
        "lateral ventricle right",
    ]
    measure_list_brainsam =["cbf10not1_brainsam"]
    measure_list_Arterial = measure_list_brainsam
    measure_list_aspects =['cbf10not1_aspects']
    res_file_path = cal_brainasl_mean(now_path, brainsam_Atlas, 'brainsam' ,measure_list_brainsam, brainasm,brainasm_label)
    cp_result_out_position(res_file_path,os.path.join(target_dir_path,"mean_measure_brainasm.xlsx"))
    res_file_path = cal_brainasl_mean(now_path, Arterial_Atlas, 'Arterial' ,measure_list_Arterial, arterial,arterial_label)
    cp_result_out_position(res_file_path,os.path.join(target_dir_path,"mean_measure_arterial.xlsx"))
    res_file_path = cal_brainasl_mean(now_path, aspects_Atlas, 'aspects' ,measure_list_aspects, aspects,aspects_label)
    cp_result_out_position(res_file_path,os.path.join(target_dir_path,"mean_measure_aspects.xlsx"))
    


def cp_data_in_position(asl_path):
    base_dir = os.path.dirname(__file__)
    new_asl_path = os.path.join(base_dir,"asl.nii.gz")
    shutil.copy(asl_path,new_asl_path)
    return new_asl_path

def cp_result_out_position(result_file_path,new_result_file_path):
    shutil.copy(result_file_path,new_result_file_path)
    return new_result_file_path



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="run asl philpis model")
    parser.add_argument('-ap',"--asl_path", type = str, help = "asl.nii.gz path")
    args = parser.parse_args()
    target_dir_path = os.path.dirname(args.asl_path)
    new_asl_path = cp_data_in_position(args.asl_path)
    run_asl(new_asl_path,target_dir_path)